// This file is part of DGGA.
//
// The MIT License (MIT)
//
// Copyright (c) 2015 Kevin Tierney and Josep Pon Farreny
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/*
 * GGAInstanceScoring.h
 * Singleton class holding all around instance rating for GGA.
 */

#ifndef GGAINSTANCESCORING_HPP_
#define GGAINSTANCESCORING_HPP_

#include "GGAInstances.hpp"
#include "GGASelector.hpp"
#include "GGAOptions.hpp"

class GGAInstanceScoring{

public:
	typedef std::map<GGAInstance, double, GGAInstance::STLMapComparator> InstancePerformancesMap;
	typedef std::map<GGAInstance, std::vector<unsigned int>, GGAInstance::STLMapComparator> InstanceVotingMap;
    // Public static methods
    //
    static GGAInstanceScoring& instance();
    static GGAInstanceScoring& mutableInstance();
    static void deleteInstance();

    // Access all the internal
    void init(const GGAInstanceVector& instances);
    //const std::vector<double>& getAllInstanceScores() const;
    const double getInstanceScore(const GGAInstance&) const;
    void setScore(const GGAInstance& pInstance, double value);
    const double& getTotalScore()const;
    void evaluate(const GGAInstanceVector& instances,const GGASelectorResult& pSelectorResult);
    void printInstanceScoringPairs();

    const std::vector<unsigned int> getInstanceVoting(const GGAInstance& pInstance);
    void addInstanceVoting(const GGAInstance& pInstance,const bool& voteForSolved);

    const std::vector<GGAInstance>& getAllSolvedInstances() const;
    const std::vector<GGAInstance>& getAllUnsolvedInstances() const;
    const std::vector<GGAInstance>& getAllTaken() const;
	const std::vector<GGAInstance>& getAllUnTaken() const;

    bool isInstanceInSolved(const GGAInstance& instance) const;
    bool isInstanceInUnsolved(const GGAInstance& instance) const;
    void resetAlreadyTakenList();

    // destruct
    virtual ~GGAInstanceScoring();

private:
    GGAInstanceScoring();
    void removeInstanceFromUnsolved(const GGAInstance& instance);
    void removeInstanceFromSolved(const GGAInstance& instance);
	void addInstanceToSolved(const GGAInstance& instance);
	void addInstanceToUnsolved(const GGAInstance& instance);
	void switchInstanceToTaken(const GGAInstance& instance);
    // Variables
    static GGAInstanceScoring* s_pInstance;
    InstancePerformancesMap m_instanceScores;
    InstanceVotingMap m_instanceVoting;
    GGAInstanceVector m_solved;
    GGAInstanceVector m_unsolved;
    GGAInstanceVector m_takenSubset;
    GGAInstanceVector m_untakenSubset;
    double totalScore;

};

//==============================================================================
// GGAOptions public inline/template static methods

/**
 *
 */
inline  GGAInstanceScoring& GGAInstanceScoring::instance()
{
    return *(s_pInstance != NULL ?
                s_pInstance : (s_pInstance = new GGAInstanceScoring()));
}

/**
 *
 */
inline GGAInstanceScoring& GGAInstanceScoring::mutableInstance()
{
    return const_cast<GGAInstanceScoring&>(GGAInstanceScoring::instance());
}

/**
 *
 */
inline void GGAInstanceScoring::deleteInstance()
{
    delete s_pInstance;
    s_pInstance = NULL;
}

inline const double GGAInstanceScoring::getInstanceScore(const GGAInstance& pInstance) const
{
	//return m_instanceScores[pInstance];
	try{
		return mapAt(m_instanceScores, pInstance);
	}catch (const std::out_of_range& oor) {
		return GGAOptions::instance().default_instance_score;
	}

}

//inline  const std::vector<double>& GGAInstanceScoring::getAllInstanceScores() const
//{
//	return m_instanceScores;
//}

inline const double& GGAInstanceScoring::getTotalScore() const
{
	return totalScore;
}
inline void GGAInstanceScoring::setScore(const GGAInstance& pInstance, double value)
{
	totalScore+=value-getInstanceScore(pInstance);
	m_instanceScores[pInstance]=value;
}


inline const std::vector<GGAInstance>& GGAInstanceScoring::getAllSolvedInstances() const
{
    return m_solved;
}

inline const std::vector<GGAInstance>& GGAInstanceScoring::getAllUnsolvedInstances() const
{
    return m_unsolved;
}

inline const std::vector<GGAInstance>& GGAInstanceScoring::getAllTaken() const
{
    return m_takenSubset;
}

inline const std::vector<GGAInstance>& GGAInstanceScoring::getAllUnTaken() const
{
    return m_untakenSubset;
}


inline bool GGAInstanceScoring::isInstanceInSolved(const GGAInstance& instance) const
{
	return std::find(m_solved.begin(), m_solved.end(), instance) != m_solved.end();
}

inline bool GGAInstanceScoring::isInstanceInUnsolved(const GGAInstance& instance) const
{
	return std::find(m_unsolved.begin(), m_unsolved.end(), instance) != m_unsolved.end();
}

inline const std::vector<unsigned int> GGAInstanceScoring::getInstanceVoting(const GGAInstance& pInstance)
{
	try{
		return mapAt(m_instanceVoting, pInstance);
	}catch (const std::out_of_range& oor) {
		std::vector<unsigned int> result(2,0);
		m_instanceVoting[pInstance]=result;
		return result;
	}
}

inline void GGAInstanceScoring::addInstanceVoting(const GGAInstance& pInstance,const bool& voteForSolved)
{
	//for initialization
	getInstanceVoting(pInstance);
	int index = voteForSolved?0:1;
	m_instanceVoting[pInstance][index]++;
}
inline void GGAInstanceScoring::switchInstanceToTaken(const GGAInstance& pInstance){
	if(std::find(m_takenSubset.begin(), m_takenSubset.end(), pInstance) == m_takenSubset.end()){
		m_untakenSubset.erase(std::remove(m_untakenSubset.begin(), m_untakenSubset.end(), pInstance), m_untakenSubset.end());
		m_takenSubset.push_back(pInstance);
	}
}
inline void GGAInstanceScoring::resetAlreadyTakenList(){
	LOG_VERY_VERBOSE("[GGAInstanceScoring] TakenList got reseted.")
	m_untakenSubset.insert(m_untakenSubset.end(), m_takenSubset.begin(), m_takenSubset.end());
	m_takenSubset.clear();
}


#endif /* GGAINSTANCESCORING_HPP_ */
