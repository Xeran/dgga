//
// This file is part of DGGA.
// 
// The MIT License (MIT)
// 
// Copyright (c) 2015 Kevin Tierney and Josep Pon Farreny
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.


#include <algorithm>
#include "GGALearningStrategy.hpp"
#include "GGAInstanceScoring.hpp"
#include "GGARandEngine.hpp"

//==============================================================================
// GGALearningStrategy public methods

/**
 * Initializes a learning strategy with some instances.
 */
GGALearningStrategy::GGALearningStrategy(const GGAInstances& instances)
    : m_instances(instances)
{
	GGAInstanceScoring::instance().init(instances.getAllInstances());
}


/**
 *
 */
GGALearningStrategy::GGALearningStrategy(const GGALearningStrategy& other)
    : m_instances(other.m_instances)
{ }


/**
 *
 */
GGALearningStrategy::~GGALearningStrategy()
{ }

/**
 * @brief Selects n instances randomly.
 */
GGAInstanceVector GGALearningStrategy::selectRandomInstances(size_t n) const
{
	return selectRandomInstances(n,m_instances.getAllInstances());
}

GGAInstanceVector GGALearningStrategy::selectRandomInstances(size_t n, const std::vector<GGAInstance>& instanceSet) const
{
    GGAInstanceVector retInsts(instanceSet);
    std::random_shuffle(retInsts.begin(), retInsts.end());
    retInsts.resize(n > retInsts.size() ? retInsts.size() : n);
    
    return retInsts;
}

GGAInstanceVector GGALearningStrategy::selectRandomClusterInstances(
        unsigned index, unsigned n) const
{
    std::vector<unsigned> cluster = m_instances.getCluster(index);
    std::random_shuffle(cluster.begin(), cluster.end());
    cluster.resize(n > cluster.size() ? cluster.size() : n);

    GGAInstanceVector retInsts;   
    for (unsigned i = 0; i < cluster.size(); ++i)
        retInsts.push_back(m_instances.getInstance(cluster[i]));
    
    return retInsts;
}
/**
 * Chooses randomly n instances depending on their instancesScoring.
 */
GGAInstanceVector GGALearningStrategy::selectWeightedRandomInstances(size_t n) const
{
	const GGAInstanceScoring& scoring = GGAInstanceScoring::instance();
	double totalScore = scoring.getTotalScore();
	std::vector<GGAInstance> result = std::vector<GGAInstance>();
	GGAInstanceVector instancesForChoose(m_instances.getAllInstances());

	for (size_t i = 0; i < n; ++i) {
		double valueToChoose = GGARandEngine::randDouble(0,totalScore);
		int index =0;
		while(valueToChoose>=0) {
			valueToChoose-=scoring.getInstanceScore(instancesForChoose[index]);
			if(valueToChoose<0){
				result.push_back(instancesForChoose[index]);
				totalScore-=scoring.getInstanceScore(instancesForChoose[index]);
				instancesForChoose.erase(instancesForChoose.begin()+index);
			}
			index++;
		}
	}
	return result;
}

