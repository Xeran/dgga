//
// This file is part of DGGA.
//
// The MIT License (MIT)
//
// Copyright (c) 2015 Kevin Tierney and Josep Pon Farreny
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

// Author: Mark Menzel

#include "ggatypedefs.hpp"
#include "GGAInstanceScoring.hpp"
#include "GGASystem.hpp"
#include "OutputLog.hpp"
#include "GGAOptions.hpp"
#include <string>
#include <iostream>
#include <sstream>

GGAInstanceScoring* GGAInstanceScoring::s_pInstance = NULL;

/**
 *
 */
GGAInstanceScoring::GGAInstanceScoring()
	:m_instanceScores()
{

}

/**
 *
 */
GGAInstanceScoring::~GGAInstanceScoring()
{ }

void GGAInstanceScoring::init(const GGAInstanceVector& instances){
	totalScore=instances.size()*GGAOptions::instance().default_instance_score;
	m_unsolved.insert(m_unsolved.end(), instances.begin(), instances.end());
	m_untakenSubset.insert(m_untakenSubset.end(), instances.begin(), instances.end());
}


/**
 * Calculates and saves the scores of the given instances.
 */
void GGAInstanceScoring::evaluate(const GGAInstanceVector& pInstances,const GGASelectorResult& pSelectorResult){
	const GGAOptions& opts = GGAOptions::instance();
	const bool switchInstancesBothWays = true;
	for (size_t i = 0; i < pInstances.size(); ++i) {
		switchInstanceToTaken(pInstances[i]);
		std::vector<double> performanceVector = pSelectorResult.getInstancePerformance(pInstances[i]);
		bool countAsSolved = false;
		std::string resultLine = "[GENERATION EVALUATION] "+pInstances[i].getInstance();
		for(std::vector<double>::iterator it = performanceVector.begin(); it != performanceVector.end(); ++it) {
			std::stringstream ss;
			ss << std::setprecision(4) << *it;
			resultLine+= " "+ ss.str();
			if(*it<opts.target_algo_cpu_limit*opts.penalty_mult){
				addInstanceVoting(pInstances[i],true);
			}else{
				addInstanceVoting(pInstances[i],false);
			}
		}
		LOG_VERY_VERBOSE(resultLine)
		unsigned int solved_votes = getInstanceVoting(pInstances[i])[0];
		unsigned int unsolved_votes = getInstanceVoting(pInstances[i])[1];
		countAsSolved=(opts.majorityVotingWeight>(float)unsolved_votes/(float)(unsolved_votes+solved_votes));
		if(countAsSolved){
			if(!isInstanceInSolved(pInstances[i])){
				removeInstanceFromUnsolved(pInstances[i]);
				addInstanceToSolved(pInstances[i]);
			}
		}else{
			//Switch Back
			if(switchInstancesBothWays&&!isInstanceInUnsolved(pInstances[i])){
				removeInstanceFromSolved(pInstances[i]);
				addInstanceToUnsolved(pInstances[i]);
			}
			//No one solved this instance
			double score = getInstanceScore(pInstances[i]);
			if(opts.score_change_unsolved){
				score+=opts.score_change_value_unsolved;
			}else{
				score*=opts.score_change_value_unsolved;
			}
			score = score<1?1:score;
			setScore(pInstances[i],score);
		}
		int testDisjunctnes = 0;
		if(isInstanceInSolved(pInstances[i])){
			testDisjunctnes+=1;
		}
		if(isInstanceInUnsolved(pInstances[i])){
			testDisjunctnes+=1;
		}
		assert(testDisjunctnes==1);
	}
	LOG_VERY_VERBOSE("[GGA INSTANCE SCORING] List of unsolved instances. ("<< m_unsolved.size() <<")")
	for (std::vector<GGAInstance>::iterator it = m_unsolved.begin(); it != m_unsolved.end(); ++it)
		LOG_VERY_VERBOSE(it->getInstance()<<" solvedVotes: "<<getInstanceVoting(*it)[0]<<" unolvedVotes: "<< getInstanceVoting(*it)[1]);
	LOG_VERY_VERBOSE("[GGA INSTANCE SCORING] List of solved instances. ("<< m_solved.size() <<")")
	for (std::vector<GGAInstance>::iterator it = m_solved.begin(); it != m_solved.end(); ++it)
		LOG_VERY_VERBOSE(it->getInstance()<<" solvedVotes: "<<getInstanceVoting(*it)[0]<<" unolvedVotes: "<< getInstanceVoting(*it)[1]);
}

void GGAInstanceScoring::printInstanceScoringPairs()
{
	LOG("Instance Scoring pairs:");
    for (std::map<GGAInstance, double>::iterator it = m_instanceScores.begin(); it != m_instanceScores.end(); ++it)
    	LOG(it->first.getInstance()<<" "<<it->second);
}

void GGAInstanceScoring::removeInstanceFromUnsolved(const GGAInstance& instance)
{
	m_unsolved.erase(std::remove(m_unsolved.begin(), m_unsolved.end(), instance), m_unsolved.end());
}
void GGAInstanceScoring::removeInstanceFromSolved(const GGAInstance& instance)
{
	m_solved.erase(std::remove(m_solved.begin(), m_solved.end(), instance), m_solved.end());
}
void GGAInstanceScoring::addInstanceToSolved(const GGAInstance& instance)
{
	m_solved.push_back(instance);
}
void GGAInstanceScoring::addInstanceToUnsolved(const GGAInstance& instance)
{
	m_unsolved.push_back(instance);
}
